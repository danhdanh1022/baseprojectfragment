package com.game.danhnguyen.lathinh.fragment.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.game.danhnguyen.lathinh.R;
import com.game.danhnguyen.lathinh.activity.FragmentInteraction;

import com.game.danhnguyen.lathinh.databinding.FragmentPlayBinding;
import com.game.danhnguyen.lathinh.fragment.BaseFragment;

/**
 * Created by DANH NGUYEN on 6/6/2018.
 */

public class PlayFragment extends BaseFragment {

    private FragmentPlayBinding mBinding;
    private FragmentInteraction mInteraction;

    public static PlayFragment newInstance() {
        Bundle args = new Bundle();
        PlayFragment fragment = new PlayFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_play, container, false);

        return mBinding.getRoot();
    }

}