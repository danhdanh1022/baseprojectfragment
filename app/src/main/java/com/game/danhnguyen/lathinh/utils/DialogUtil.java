package com.game.danhnguyen.lathinh.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.game.danhnguyen.lathinh.R;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class DialogUtil {

    private static Dialog mdialog;

    public interface ListenerOnClickButtonPopup {
        void onClickButtonLeft(String content);

        void onClickButtonRight(String content);
    }

    public interface ListenerOnCancelPopup extends DialogInterface.OnCancelListener {
        @Override
        void onCancel(DialogInterface dialogInterface);
    }

    ListenerOnClickButtonPopup listenerOnClickButtonPopup;

    public static void dismiss() {
        if (mdialog != null && mdialog.isShowing()) {
            mdialog.dismiss();
            mdialog = null;
        }
    }

    public static void showBasicDialogIsCancel(Context context, String title, String message, View.OnClickListener onClickListener , DialogUtil.ListenerOnCancelPopup onCancelListener) {
        dismiss();
        mdialog = new Dialog(context);
        mdialog.setCanceledOnTouchOutside(true);
        mdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        FontUtil font = new FontUtil(context);
        mdialog.setOnCancelListener(onCancelListener);
        mdialog.setContentView(R.layout.dialog_warning_basic);

        TextView title1 = mdialog.findViewById(R.id.text_dialog_title);
        TextView content = mdialog.findViewById(R.id.text_dialog_content);
        Button btnOK = mdialog.findViewById(R.id.text_dialog_done);

        title1.setText(title);
        content.setText(message);

        btnOK.setOnClickListener(onClickListener);
        font.setFont(FontUtil.FRUTIGER_LIGHT_FONT, content);
        font.setFont(FontUtil.FRUTIGER_ROMAN_FONT, btnOK);
        font.setFont(FontUtil.FRUTIGER_BOLD_FONT, title1);
        if (mdialog != null && !mdialog.isShowing())
            mdialog.show();
    }
}
