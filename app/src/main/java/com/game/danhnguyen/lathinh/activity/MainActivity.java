package com.game.danhnguyen.lathinh.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.game.danhnguyen.lathinh.R;
import com.game.danhnguyen.lathinh.activity.BaseActivity;
import com.game.danhnguyen.lathinh.extras.Enumes;
import com.game.danhnguyen.lathinh.fragment.BaseFragment;
import com.game.danhnguyen.lathinh.fragment.view.HomeFragment;
import com.game.danhnguyen.lathinh.fragment.view.PlayFragment;

public class MainActivity extends BaseActivity implements FragmentInteraction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        BaseFragment fragment = null;
        fragment = new HomeFragment();
        transaction.replace(R.id.activity_fragment_container, fragment);
        transaction.commit();


    }


    @Override
    public void openFragment(Enumes.FragmentType fragmentType, Bundle bundle, Enumes.Direction directionIn) {
        hideKeyboard();

        BaseFragment fragment = null;
        switch (fragmentType) {
            case FRAGMENT_HOME:
                fragment = new HomeFragment();
                break;
            case FRAGMENT_PLAY:
                fragment = new PlayFragment();
                break;
        }

        if (fragment == null) {
            return;
        }

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        switch (directionIn) {
            case LEFT_IN:
                break;
            case RIGHT_IN:
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case RIGHT_IN_FLIP_OUT:
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.from_middle, R.anim.to_middle);
                break;
            case TOP_IN:
                transaction.setCustomAnimations(R.anim.enter_from_top, R.anim.exit_to_bottom);
                break;
            case BOTTOM_IN:
                break;
            case FLIP_IN:
                transaction.setCustomAnimations(R.anim.from_middle, R.anim.to_middle);
                break;
            case FLIP_OUT:
                transaction.setCustomAnimations(R.anim.from_middle, R.anim.to_middle);
                break;
        }

        if (bundle != null && bundle.size() > 0) {
            fragment.setArguments(bundle);
        }

        String tagName = fragmentType.toString();
        transaction.replace(R.id.activity_fragment_container, fragment, tagName);

        int backStackCount = manager.getBackStackEntryCount();
        Log.d("Fragment", "backStackCount " + backStackCount);
        if (backStackCount > 0) {
            transaction.addToBackStack(tagName);
        } else {
            transaction.addToBackStack(tagName);
        }

        transaction.commit();
    }
}
