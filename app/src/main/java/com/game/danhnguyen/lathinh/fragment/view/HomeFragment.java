package com.game.danhnguyen.lathinh.fragment.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.game.danhnguyen.lathinh.R;
import com.game.danhnguyen.lathinh.activity.FragmentInteraction;
import com.game.danhnguyen.lathinh.databinding.FragmentHomeBinding;
import com.game.danhnguyen.lathinh.extras.Enumes;
import com.game.danhnguyen.lathinh.fragment.BaseFragment;

/**
 * Created by DANH NGUYEN on 6/6/2018.
 */

public class HomeFragment extends BaseFragment {

    private FragmentHomeBinding mBinding;
    private FragmentInteraction mInteraction;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mInteraction = (FragmentInteraction) context;
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        init();
        return mBinding.getRoot();
    }

    private void init(){
        mBinding.btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInteraction.openFragment(Enumes.FragmentType.FRAGMENT_PLAY,null, Enumes.Direction.RIGHT_IN);
            }
        });
    }



}
