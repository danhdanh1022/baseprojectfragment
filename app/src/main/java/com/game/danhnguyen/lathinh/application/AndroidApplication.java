package com.game.danhnguyen.lathinh.application;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.multidex.MultiDex;

import com.game.danhnguyen.lathinh.utils.LocaleHelper;

import java.util.Locale;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class AndroidApplication extends Application {
    private static AndroidApplication mInstance;
    public String OS;
    public String Version;
    public String language;

    public static synchronized AndroidApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        initAppInformation();
    }

    private void initAppInformation() {
        OS = Build.VERSION.RELEASE;
        PackageInfo pInfo = null;
        try {pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {e.printStackTrace();}
        Version = pInfo.versionName;
        language = Locale.getDefault().getLanguage();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "vn"));
        MultiDex.install(this);
    }


}
