package com.game.danhnguyen.lathinh.extras;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class Enumes {
    public enum FragmentType {

        FRAGMENT_DEFAULT_NULL("FRAGMENT_DEFAULT_NULL"),
        FRAGMENT_ROOM_CENTRE("FRAGMENT_ROOM_CENTRE"),
        FRAGMENT_HOME("FRAGMENT_HOME"),
        FRAGMENT_PLAY("FRAGMENT_PLAY");

        final String mValue;

        FragmentType(String value) {
            mValue = value;
        }

        public String toString() {
            return mValue;
        }
    }

    public enum Direction {
        NONE("NONE"),
        LEFT_IN("LEFT_IN"),
        RIGHT_IN("RIGHT_IN"),
        RIGHT_IN_FLIP_OUT("RIGHT_IN_FLIP_OUT"),
        TOP_IN("TOP_IN"),
        BOTTOM_IN("BOTTOM_IN"),
        FLIP_IN("FLIP_IN"),
        FLIP_OUT("FLIP_OUT");

        final String mDirection;

        Direction(String value) {
            mDirection = value;
        }

        public String toString() {
            return mDirection;
        }
    }
}
