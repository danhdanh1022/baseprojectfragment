package com.game.danhnguyen.lathinh.application;

import android.content.Context;
import android.content.SharedPreferences;

import com.game.danhnguyen.lathinh.R;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class AndroidPreference {
    public final String KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN";
    public final String KEY_USER_PROFILE = "KEY_USER_PROFILE";

    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private Context mContext;

    public AndroidPreference(Context context) {
        this.mContext = context;
        this.pref = mContext.getSharedPreferences(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        this.editor = pref.edit();
    }

    public void commit() {
        editor.commit();
    }

    public void saveAccessToken(String accessToken) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(KEY_ACCESS_TOKEN, Context.MODE_PRIVATE).edit();
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.apply();
    }

    public String getAccessToken() {
        return mContext.getSharedPreferences(KEY_ACCESS_TOKEN, Context.MODE_PRIVATE).getString(KEY_ACCESS_TOKEN, "");
    }

    public void clearToken() {
        mContext.getSharedPreferences(KEY_ACCESS_TOKEN, Context.MODE_PRIVATE).edit().clear().apply();
    }

    public void saveUserProfile(String profileJson) {
        editor.putString(KEY_USER_PROFILE, profileJson);
        editor.commit();
    }

    public String loadUserProfile() {
        return pref.getString(KEY_USER_PROFILE, "");
    }
}
