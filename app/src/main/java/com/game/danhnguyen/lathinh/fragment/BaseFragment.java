package com.game.danhnguyen.lathinh.fragment;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.game.danhnguyen.lathinh.activity.BaseActivity;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class BaseFragment extends Fragment {

    protected void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void onBackAndHideKeyBoard() {
        hideKeyboard(getContext());
        getActivity().onBackPressed();
    }
}