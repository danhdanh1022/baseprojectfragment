package com.game.danhnguyen.lathinh.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Hashtable;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public class FontUtil {

    public static String FONT_TYPE_LATO_REGULAR = "fonts/lato-regular.ttf";

    public static String FRUTIGER_LIGHT_FONT = "fonts/Frutiger45-Light.ttf";
    public static String FRUTIGER_ROMAN_FONT = "fonts/Frutiger55Roman.ttf";
    public static String FRUTIGER_BOLD_FONT = "fonts/Frutiger65-Bold.ttf";

    private Context mContext;

    public FontUtil(Context context) {
        this.mContext = context;
    }

    private static Hashtable<String, Typeface> sTypeFaces = new Hashtable<String, Typeface>(
            5);

    public Typeface getTypeFace(String font) {
        Typeface tempTypeface = sTypeFaces.get(font);

        if (tempTypeface == null) {
            tempTypeface = Typeface.createFromAsset(mContext.getAssets(), font);
            sTypeFaces.put(font, tempTypeface);
        }

        return tempTypeface;
    }

    public void setFont(String fontName, View view) {
        if (mContext != null && view != null) {
            if (view instanceof TextView)
                ((TextView) view).setTypeface(getTypeFace(fontName));

            if (view instanceof Button)
                ((Button) view).setTypeface(getTypeFace(fontName));

            if (view instanceof EditText)
                ((EditText) view).setTypeface(getTypeFace(fontName));

            if (view instanceof CheckBox)
                ((CheckBox) view).setTypeface(getTypeFace(fontName));
        }
    }


}

