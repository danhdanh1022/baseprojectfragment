package com.game.danhnguyen.lathinh.activity;

import android.os.Bundle;

import com.game.danhnguyen.lathinh.extras.Enumes;

/**
 * Created by DANH NGUYEN on 6/5/2018.
 */

public interface FragmentInteraction {
    void openFragment(Enumes.FragmentType fragmentType, Bundle bundle, Enumes.Direction directionIn);

}
